<?php

namespace Drupal\fitbit;

use djchen\OAuth2\Client\Provider\Fitbit;
use djchen\OAuth2\Client\Provider\FitbitUser;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use Drupal\Core\Utility\Error;
use Psr\Log\LogLevel;

/**
 * Fitbit client wrapper. Implement custom methods to retrieve specific Fitbit
 * data using access_tokens stored in Drupal.
 */
class FitbitClient extends Fitbit {
  use StringTranslationTrait;

  /**
   * Header value to pass along for Accept-Languge, which toggles between the
   * allowed unit systems.
   *
   * @var string
   */
  protected $acceptLang;

  /**
   * FitbitClient constructor.
   *
   * @param array $options
   * @param string $accept_lang
   */
  public function __construct(array $options, $accept_lang = NULL) {
    parent::__construct($options);
    $this->setAcceptLang($accept_lang);
  }

  /**
   * Setter for the value of the Accept-Language header in all Fitbit profile
   * requests.
   *
   * @param string $accept_lang
   */
  public function setAcceptLang($accept_lang = NULL) {
    $this->acceptLang = $accept_lang;
  }

  /**
   * Get the resource owner by Drupal uid.
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @return FitbitUser|null
   */
  public function getResourceOwner(AccessToken $access_token) {
    if ($response = $this->request('/1/user/-/profile.json', $access_token)) {
      return new FitbitUser($response);
    }
  }

  /**
   * Get a users badges.
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @return mixed|null
   */
  public function getBadges(AccessToken $access_token) {
    return $this->request('/1/user/-/badges.json', $access_token);
  }

  /**
   * Get detailed heart rate data for the logged in user. Heart rate differs from other
   * activities as it supports data on the 1 second granularity (even though the reported data is
   * in 3-4 second intervals).
   * https://dev.fitbit.com/build/reference/web-api/intraday/get-heartrate-intraday-by-interval/
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @param string $startDate
   * @param string $endDate
   * @param string $startTime
   * @param string $endTime
   * @param string $detailLevel
   *
   * @return mixed|null
   */
  public function getHeartRateIntradayByInterval(AccessToken $accessToken, $startDate, $endDate, $startTime = NULL, $endTime = NULL, $detailLevel = '1sec') {
    return $this->getActivityIntradayByInterval($accessToken, $startDate, $endDate, $startTime, $endTime, 'heart', $detailLevel);
  }

  /**
   * Get detailed activity intraday date. Special permissions (personal token) are required
   * for this, but it differs from getActivityLogList by allowing you to specify start end
   * end times.
   * https://dev.fitbit.com/build/reference/web-api/intraday/get-activity-intraday-by-interval/
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @param string $startDate
   * @param string $endDate
   * @param string $startTime
   * @param string $endTime
   * @param string $detailLevel
   *
   * @return mixed|null
   */
  public function getActivityIntradayByInterval(AccessToken $accessToken, $startDate, $endDate, $startTime = NULL, $endTime = NULL, $resource = 'calories', $detailLevel = '1min') {
    $timeUrl = NULL;
    $acceptedIntervals = ['1min', '5min', '15min'];
    $heartRateAcceptedIntervals = ['1sec', '1min', '5min', '15min'];

    if ($resource != 'heart' && !in_array($detailLevel, $acceptedIntervals)) {
      \Drupal::logger('fitbit')->alert('Invalid detail level (' . $detailLevel . ') for requested resource (' . $resource . ')');
      return [];
    }
    elseif ($resource == 'heart' && !in_array($detailLevel, $heartRateAcceptedIntervals)) {
       \Drupal::logger('fitbit')->alert('Invalid detail level (' . $detailLevel . ') for requested resource (' . $resource . ')');
      return [];
    }

    if (!empty($startTime)) {
      if (empty($endTime)) {
        $endTime = '23:59';
      }
      $timeUrl = '/time/' . $startTime . '/' . $endTime;
    }
    $date = '/date/' . $startDate . '/' . $endDate . '/' . $detailLevel;
    $data = $this->request('/1/user/-/activities/' . $resource . $date . $timeUrl . '.json', $accessToken);
    return $data;
  }


  /**
   * Get the heart rate time series for a particular day. Can be used to get the resting
   * heart rate for a particular date. Not as detailed as intervals, but good for zones.
   * https://dev.fitbit.com/build/reference/web-api/heartrate-timeseries/get-heartrate-timeseries-by-date/
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @param string $date
   * @param string $interval
   *
   * @return mixed|null
   */
  public function getHeartRateTimeSeries(AccessToken $accessToken, $date, $interval = '1d') {
    $acceptedIntervals = ['1d', '7d', '30d', '1w', '1m'];

    if (!in_array($interval, $acceptedIntervals)) {
      \Drupal::logger('fitbit')->alert('Invalid interval (' . $interval . ') for heart rate time series.');
      return [];
    }

    $data = $this->request('/1/user/-/activities/heart/date/' . $date . '/' . $interval . '.json', $accessToken);
    return $data;
  }

  /**
   * Get daily activity for the given user.
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   * @param string $date
   *
   * @return mixed
   */
  public function getDailyActivitySummary(AccessToken $access_token, $date = NULL) {
    if (!isset($date)) {
      $date = date('Y-m-d', REQUEST_TIME);
    }
    return $this->request('/1/user/-/activities/date/' . $date . '.json', $access_token);
  }

  /**
   * Get a summary of VO2 Max values for a date range specified. This is normally just one day
   * but we want the API to have the option for multiple days.
   *
   * Reference:
   * https://dev.fitbit.com/build/reference/web-api/cardio-fitness-score/get-vo2max-summary-by-interval/
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   * @param string $startDate
   * @param string $endDate
   *
   * @return mixed
   */
  public function getVO2MaxSummaryByInterval(AccessToken $accessToken, $startDate, $endDate) {
    $data = $this->request('/1/user/-/cardioscore/date/' . $startDate . '/' . $endDate . '.json', $accessToken);
    return $data;
  }

  public function getWithFormedUrl(AccessToken $access_token, $url) {
    $opts = [
      'http' => [
        'method' => "GET",
        'header' => "Authorization: Bearer " . $access_token . "\r\n" . "Accept-Language: en_US\r\n",
      ],
    ];
    $context = stream_context_create($opts);
    $contents = file_get_contents($url, FALSE, $context);
    return $contents;
  }

  /**
   * Get activity log list.
   * https://dev.fitbit.com/build/reference/web-api/activity/get-activity-log-list
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @param string $afterDate
   * @param string $sort
   * @param int $offset
   * @param int $limit
   *
   * @return mixed
   */
  public function getActivityLogList(AccessToken $access_token, $afterDate = NULL, $sort = 'asc', $offset = 0, $limit = 5) {
    if (!isset($afterDate)) {
      $afterDate = date('Y-m-d', REQUEST_TIME);
    }
    return $this->request('/1/user/-/activities/list.json?afterDate=' . $afterDate . '&sort=' . $sort . '&offset=' . $offset . '&limit=' . $limit, $access_token);
  }

  /**
   * Get activity time series.
   *
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   * @param $resource_path
   *   One of the allowable resource paths accepted by the Fitbit API, for
   *   example, activities/steps. For the full list, see
   *   https://dev.fitbit.com/docs/activity/#resource-path-options
   * @param array $activity_date_range
   *   Associative array with keys. Use date + period for a period based range,
   *   base_date and end_date for a specific date range:
   *   - date
   *   - period
   *   - base_date
   *   - end_date
   *   See https://dev.fitbit.com/docs/activity/#activity-time-series for more details.
   *
   * @return mixed
   */
  public function getActivityTimeSeries(AccessToken $access_token, $resource_path = NULL, $activity_date_range = []) {
    isset($resource_path) ?: $resource_path = 'activities/steps';
    if (!empty($activity_date_range['base_date']) && !empty($activity_date_range['end_date'])) {
      $activity_date_range = $activity_date_range['base_date'] . '/' . $activity_date_range['end_date'];
    }
    else {
      $activity_date_range = (isset($activity_date_range['date']) ? $activity_date_range['date'] : 'today') . '/' . (isset($activity_date_range['period']) ? $activity_date_range['period'] : '7d');
    }
    return $this->request('/1/user/-/' . $resource_path . '/date/' . $activity_date_range . '.json', $access_token);
  }

  /**
   * Request a resource on the Fitbit API.
   *
   * @param string $resource
   *   Path to the resource on the API. Should include a leading /.
   * @param AccessToken $access_token
   *   Fitbit AccessToken object.
   *
   * @return mixed|null
   *   API response or null in the case of an exception, which can happen if the
   *   user did not authorize the resource being requested.
   */
  public function request($resource, AccessToken $access_token) {
    $options = [];
    if ($this->acceptLang) {
      $options['headers'][Fitbit::HEADER_ACCEPT_LANG] = $this->acceptLang;
    }
    $request = $this->getAuthenticatedRequest(
      Fitbit::METHOD_GET,
      Fitbit::BASE_FITBIT_API_URL . $resource,
      $access_token,
      $options
    );

    try {
      return $this->getResponse($request);
    }
    catch (IdentityProviderException $e) {
      $log_level = RfcLogLevel::ERROR;
      // Look through the errors reported in the response body. If the only
      // error was an insufficient_scope error, report as a notice.
      $parsed = $this->parseResponse($e->getResponseBody());
      if (!empty($parsed['errors'])) {
        $error_types = [];
        foreach ($parsed['errors'] as $error) {
          if (isset($error['errorType'])) {
            $error_types[] = $error['errorType'];
          }
        }
        $error_types = array_unique($error_types);
        if (count($error_types) === 1 && reset($error_types) === 'insufficient_scope') {
          $log_level = RfcLogLevel::NOTICE;
        }
      }

      $logger = \Drupal::logger('fitbit');
      Error::logException($logger, $exception, NULL, [], $log_level);
      return FALSE;
    }
  }

  /**
   * Return an array of supported values for Accept-Language, which correspond
   * to the unit systems supported by the API.
   *
   * @return array
   *   Associative array keyed by Accept-Language header value. Each value is
   *   the name of the units system.
   */
  public function getAcceptLangOptions() {
    return [
      '' => $this->t('Metric'),
      'en_US' => $this->t('US'),
      'en_GB' => $this->t('UK'),
    ];
  }

  /**
   * Ensure that the redirectUri param is set. Way to get around inability to
   * use Url::toString() during a router rebuild.
   */
  protected function ensureRedirectUri() {
    if (!isset($this->redirectUri)) {
      $this->redirectUri = Url::fromRoute('fitbit.authorization', [], ['absolute' => TRUE])->toString();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getAuthorizationParameters(array $options) {
    $this->ensureRedirectUri();
    return parent::getAuthorizationParameters($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken($grant, array $options = []) {
    $this->ensureRedirectUri();
    return parent::getAccessToken($grant, $options);
  }
}
